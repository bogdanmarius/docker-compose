## ownCloud repository

## Adapt the docker-compose.yml file as needed
Use the .enf file to add sensitive values
Also for OWNCLOUD_TRUSTED_DOMAINS=localhost, replace it with the desired value, either domain or ip address(public or private).

## More details about Docker installation
https://doc.owncloud.com/server/10.13/admin_manual/installation/docker/

## Starting the containers
docker-compose -f docker-compose.yml up -d

## Stoping the containers
docker-compose -f docker-compose.yml down
