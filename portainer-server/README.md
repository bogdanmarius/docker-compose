## portainer-server repository

## Install Portainer CE with Docker on Linux
https://docs.portainer.io/start/install-ce/server/docker/linux

## Github link
https://github.com/portainer/portainer

## Initial setup
https://docs.portainer.io/start/install-ce/server/setup

## Start the container
docker-compose -f docker-compose.yaml up -d

## Shutdown the container
docker-compose -f docker-compose.yaml down

## Access the interface and create the admin user
https://127.0.0.1:9443/#!/home