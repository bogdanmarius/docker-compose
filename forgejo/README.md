## Forgejo - Git server - repository

## Project details:
About Forgejo: https://forgejo.org/

## Project releases:
https://forgejo.org/releases/

## Docker installation modes:
Link: https://forgejo.org/docs/latest/admin/installation-docker/

## Docker-compose settings:
Use the defaults for testing, adapt to match security requirements for production environments.

## Running the services:
start: docker-compose -f docker-compose.yaml up -d
stop: docker-compose -f docker-compose.yaml down

## Access the platform:
link: http://localhost:3000

## Configuration:
The first time the service is access the configuration page is displayed. This will allow for platform connfiguration and admin user creation.
