#!/bin/bash
# Author: Marius Bogdan Ursan
# Date: 09/11/2023
# Description Bash script to setup Jenkins CI/CD
# Version 1.0
# Date modified: 09/11/2023

echo 'Setup script for jenkins using docker-compose'
echo 'User with sudo access required'
echo ' ===== '
echo 'Create working directory and set ownership'
sudo mkdir jenkins_home
sudo chown -R 1000:1000 jenkins_home/
echo ' ===== '
ls -all |grep jenkins
echo 'Starting the jenkins container'
echo 'Running this as local user'
docker-compose -f docker-compose.yaml up -d
echo 'Execution check'
docker ps
echo ' DONE '
