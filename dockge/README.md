# Welcome to dockge repo

## What is Dockge
Dockge is Self-hosted - Docker compose.yaml - Stack-oriented Manager

## Generate your custom compose yaml file from the website
https://dockge.kuma.pet/

## Project details
https://github.com/louislam/dockge

## Managing the dockge instance using docker-compose binary
docker-compose -f docker-compose.yaml up -d
<p>docker-compose -f docker-compose.yaml down</p>

## Access the instance
http://localhost:5001
<p>Create an admin user at first start-up to use dockge