# Define operating system
FROM almalinux/8-base

# Labels
LABEL org.opencontainers.image.authors="mail@example.com"
LABEL org.opencontainers.image.version="1.0"
LABEL org.opencontainers.image.description="AlmaLinux8 with ssh server"

# Update operating system and install openssh-server
RUN dnf update -y
RUN dnf install -y procps openssh-server openssh-clients sudo

# Install epel and some system tools
RUN dnf install -y epel-release
RUN dnf install -y nano screen wget ftp telnet bind-utils whois traceroute vim tar zip unzip htop iptraf-ng net-tools

# Configure SSH access for ROOT
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config \
    && echo "root:Docker@2023" | chpasswd

# Configure PubkeyAuthentication
RUN sed -i 's/#PubkeyAuthentication yes/PubkeyAuthentication yes/' /etc/ssh/sshd_config

# Generate keys
RUN ssh-keygen -A

# Configure SSH access for USER with sudo rights
RUN useradd -rm -d /home/username -s /bin/bash -g root -G wheel -u 1000 username

# Define password for the user
RUN echo 'username:password@2023' | chpasswd

# Expose the SSH port
EXPOSE 22

# Start the SSH server
CMD ["/usr/sbin/sshd", "-D"]
