## <p><b>Elasticsearch Stack Version 8.12</b></p>

<br>Use the repository as model, <b> don`t use it for production deployment</b>.

## Be aware of the following details:
<p> - adjust the user and password defined to suit your needs
<p> - adjust in filebeat-config / filebeat.yml -> logging.level: debug instead of info to check for problems
<p> - the Stack is configured to send output to elasticsearch.

## Setup the kibana user
 reference
 <p> https://www.elastic.co/guide/en/elasticsearch/reference/8.12/built-in-users.html
 <p> After the containers are started we setup the reserved used for this testing case kibana_system
 <p> curl -X POST "http://127.0.0.1:9200/_security/user/kibana_system/_password" -H 'Content-Type: application/json' -u elastic:ELKstack@2023 -d '{ "password": "KIBANAstack@2023" }'
 <p> curl -X GET "http://127.0.0.1:9200/_cat/health?v" -u elastic:ELKstack@2023

## Kibana health check
<p> curl -X GET http://127.0.0.1:5601/api/status

## Login details:
 login details for kibana interface: user elastic with password ELKstack@2023
