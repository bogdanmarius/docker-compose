## Gitea - Git server - repository

## Project details:
About Gitea: https://about.gitea.com/

## Docker installation modes:
Link: https://docs.gitea.com/installation/install-with-docker

## Docker-compose settings:
Use the defaults for testing, adapt to match security requirements for production environments.

## Running the services:
start: docker-compose -f docker-compose.yaml up -d
stop: docker-compose -f docker-compose.yaml down

## Access the platform:
link: http://localhost:3000

## Configuration:
After the services are start the first access to the platform will allow to configure parameters and the admin account.
