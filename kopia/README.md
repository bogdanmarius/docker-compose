## kopia backup docker-compose repo
<p>website: https://kopia.io/</p>
documentation: https://kopia.io/docs/installation/#docker-images

## using the container
<p>docker-compose -f docker-compose.yaml up -d</p>
docker-compose -f docker-compose.yaml down

## CLI usage
https://kopia.io/docs/reference/command-line/