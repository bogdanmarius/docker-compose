<p>CODE-SERVER SETUP</p>

<p><b>Project web page:</b> https://coder.com/docs/code-server/latest </p>

<p><b>Almalinux 8 code-server setup of working directories:</b>
<br>Logged in as normal user execute the following commands:
<br>mkdir -p ~/.config
<br>mkdir -p ~/.share

<b>START THE CONTAINER with proper ENV options:</b>
<br>in interactive mode:
<br>`env UID=${UID} GID=${GID} docker-compose -f docker-compose.yaml up`

<br>in detached mode:
<br>`env UID=${UID} GID=${GID} docker-compose -f docker-compose.yaml up -d`

<br>shutdown and clean:
<br>`env UID=${UID} GID=${GID} docker-compose -f docker-compose.yaml down`</p>

<p><b>Integration with Apache Web server via reverse proxy:</b>
https://gitlab.com/bogdanmarius/httpd-conf/-/tree/master/code-server?ref_type=heads </p>
