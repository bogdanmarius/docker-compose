#!/bin/bash
# Author: Marius Bogdan Ursan
# Date: 16/11/2023
# Description Bash script to setup Opensearch
# Version 1.0
# Date modified: 16/11/2023

echo -e 'Welcome to Opensearch setup script
\nThis is used to start services 
\nand restart fluent-bit service to reconnect with data-prepper
\n===============
\nStarting services please wait'


docker-compose -f docker-compose.yml up -d
echo -e '===============
\nWaiting for 30 seconds of uptime'
sleep 30
docker ps
echo -e 'Please wait for log checks
\n==============='

echo 'Checking data-prepper startup log'
docker logs data-prepper
echo '==============='

echo 'Checking fluent-bit logs'
docker logs fluent-bit
echo '==============='

echo 'Restarting fluent-bit container'
docker-compose restart fluent-bit
echo -e 'Waiting for 20 seconds of uptime
\n==============='
sleep 20

echo 'reChecking fluent-bit logs for 200 OK'
docker logs fluent-bit
echo '==============='

echo 'Setup and execution complete'
docker ps
echo '==============='
echo 'To power down the containers use: docker-compose down'
