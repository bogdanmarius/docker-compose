<p><b>OpenSearch repository</b> https://opensearch.org/ </p>

<p><b>Resources and examples:</b></p>
Index settings: https://opensearch.org/docs/2.11/im-plugin/index-settings/
<p>Pipelines models: https://opensearch.org/docs/latest/data-prepper/pipelines/pipelines/
<br>Examples: https://github.com/opensearch-project/data-prepper/tree/main/examples/log-ingestion

<p> <b>Execute the bash script</b> to start the services. Data-prepper services takes some time to start
that`s why fluent-bit needs to be restarted again</p>

<p>Login details: admin/admin</p>
Adjust them as needed.

<br>Login link: http://localhost:5601/ or http://ip-address:5601/
