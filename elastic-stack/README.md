<p><b>Elasticsearch Stack Version 7.17</b></p>

<br>Use the repository as model, <b> don`t use it for production deployment</b>.

Be aware of the following details:
<p> - adjust the user and password defined to suit your needs
<p> - adjust in filebeat-config / filebeat.yml -> logging.level: debug instead of info to check for problems
<p> - the Stack is configured to send output to elasticsearch.
