**Welcome to openspeedtest repository**

**Website: https://openspeedtest.com/?ref=logo**

Github project repo: https://github.com/openspeedtest/Speed-Test

Docker image: https://hub.docker.com/r/openspeedtest/latest

**Execution:**

to start the container: 
docker-compose -f name-of-file.yml up -d

to stop the container: 
docker-compose -f name-of-file.yml down

**Accessing the test:**

http://localhost:3000

https://localhost:3001 (accept the self signed certificate)
