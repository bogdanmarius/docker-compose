#!/bin/bash
# Author: Marius Bogdan Ursan
# Date: 08/11/2023
# Description Bash script to setup DomainMod
# Version 1.0
# Date modified: 08/11/2023

echo 'Welcome to DomainMod setup script'
echo '====='

echo 'Creating working directories'

mkdir application
mkdir database

echo 'Created working directories: application database'
echo '====='

echo 'Starting the docker containers'
docker-compose -v
echo '====='
docker-compose up -d

echo 'Setup and execution complete'
docker ps
echo 'To power down the containers use: docker-compose down'
