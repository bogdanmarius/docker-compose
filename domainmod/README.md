<p>DomainMOD setup</p>

<p><b>Project web page:</b> https://hub.docker.com/r/domainmod/domainmod </p>

<p><b>DomainMOD setup:</b>
<br>adjust the docker-compose.yaml file with proper login details
<br>execute the bash script to start the containers

<b>START THE CONTAINER as desired:</b>
<br>in interactive mode:
<br>`docker-compose -f docker-compose.yaml up`

<br>in detached mode:
<br>`docker-compose -f docker-compose.yaml up -d`

<br>shutdown and clean:
<br>`docker-compose -f docker-compose.yaml down`</p>
