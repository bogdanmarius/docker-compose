## Uptime-kuma repo

# Program website:
https://uptime.kuma.pet/

# Github repository:
https://github.com/louislam/uptime-kuma

# Execution using docker command:
docker run -d --restart=always -p 3001:3001 -v uptime-kuma:/app/data --name uptime-kuma louislam/uptime-kuma:1

# Execution using docker-compose command:
clone the repo, access the uptime-kuma directory
<p>docker-compose -f docker-compose.yaml up -d</p>
<p>docker-compose -f docker-compose.yaml down</p>

# Web access at:
http://localhost:3001
<p>http://ip-address:3001</p>