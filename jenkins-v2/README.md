# Updated Jenkins deployment

## download the yaml file and start the container
docker-compose -f docker-compose.yaml up -d

## initial login
use this command to fetch the initial password: docker logs jenkins | less 

## stop the container
docker-compose down