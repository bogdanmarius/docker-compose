## Heimdall Application Dashboard
Website: https://heimdall.site/

## Usage of docker compose file
docker-compose -f docker-compose.yaml up -d

## Login details
The user admin is created automatically without a password. 
<p>Create a new user to add security to your dashboard.</p>

## To stop the dashboard container use
docker-compose -f docker-compose.yaml stop
