<p><b>Grafana repository </b></p>

Grafana yml file with create the services with mount point for /var/log/ to ingest and read logs from localhost. 

The full yml file with create all services (including prometheus and node_exporter) to ingest logs and to have monitoring capability .

<b>Login details</b>: defaults admin / admin . Adapt as needed.

<b>Login link</b>: http://localhost:3000/ or http://ip-address:3000/

<b>Dashboard to install</b>: import dashboard with id 1860
