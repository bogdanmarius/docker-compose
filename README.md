# docker-compose



## Clone the repository

```
cd local_folder
git clone https://gitlab.com/bogdanmarius/docker-compose.git
cd docker-compose
```

## Repository details

This repository contains predefined docker-compose.yaml files for several types of operating systems.
Adapt as need, don`t use the defaults.


## Project status
Project is active
