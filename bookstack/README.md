## Welcome to bookstack
Bookstack is a free and open source alternative to Confluence
<p>Website: https://www.bookstackapp.com/
<p>Github: https://github.com/linuxserver/docker-bookstack/tree/master

## Starting the application
docker-compose -f docker-compose.yaml up -d

## Stoping the application
docker-compose -f docker-compose.yaml stop

## Accessing the platform with default login details
The default username is admin@admin.com with the password of password, access the container at http://dockerhost:6875 or http://ip-address:6875 .
