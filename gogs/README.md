## Gogs - self hosted GIT repository service

## What is Gogs
Website: https://gogs.io/

## Docker instruction
Github page: https://github.com/gogs/gogs/tree/main/docker

## Adapt and use the docker-compose as you wish
The variables that Gogs use don`t include database login details.
The database details for the installation must be manually inserted at http://localhost:3000/install.
<p>Example:<br>
      - POSTGRES_USER=gogs-user<br>
      - POSTGRES_PASSWORD=gogs-password<br>
      - POSTGRES_DB=gogs-db

## Usage of Gogs
The first user created will be granted admin rights
Access http://localhost:3000 and Register

## Database management - Adminer
Access adminer at http://localhost:8080
