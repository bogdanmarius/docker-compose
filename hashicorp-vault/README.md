# hashicorp-vault

## setup local folders and config
clone the repo and adapt as needed

## start the container
docker-compose -f docker-compose.yaml up -d

## get the unseal keys and root token, save the details
docker exec -it vault sh -c 'VAULT_ADDR="http://127.0.0.1:8200" vault operator init'

## shutdown the container
docker-compose -f docker-compose.yaml down

## final warning
Dont use this setup for production. Protect the setup by using nginx/apache web server for reverse proxy.
