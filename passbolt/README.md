<b>Passbolt - open source password manager built for organizations</b>

Links with details: 
https://help.passbolt.com/hosting/install/ce/docker.html
<p>https://hub.docker.com/r/passbolt/passbolt

WARNING: the sha512sum will fail because the yaml file has extra comments added, remote those before validating

Container management:
<p>start: docker-compose -f docker-compose-ce.yaml up -d
<p>stop: docker-compose -f docker-compose-ce.yaml down

Add an initial user:
<p>docker exec passbolt-passbolt-1 su -m -c "bin/cake passbolt register_user -u demo@email.com -f demo -l account -r admin" -s /bin/sh www-data

Accessing passbolt:
<p>http using: http://localhost

Have fun testing ! :) 
